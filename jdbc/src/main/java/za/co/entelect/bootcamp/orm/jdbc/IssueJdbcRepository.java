package za.co.entelect.bootcamp.orm.jdbc;

import za.co.entelect.bootcamp.orm.core.common.Page;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.gateway.exception.NotFoundException;
import za.co.entelect.bootcamp.orm.core.gateway.exception.RepositoryException;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readpage.ReadIssuePageInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SqlResolve")

public class IssueJdbcRepository
        implements ReadOneIssueInterface, ReadIssuePageInterface, SaveIssueInterface {
    private final Connection connection;

    public IssueJdbcRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public IssueDTO findById(Long id) {
        try {
            String query =
                    "SELECT IssueID, Title, PublicationDate, Publisher, SeriesNumber, Description " +
                            "FROM Issues " +
                            "WHERE IssueId = " + id;

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            if (!resultSet.next()) {
                throw new NotFoundException(IssueDTO.class, id);
            }

            IssueDTO result = mapRow(resultSet);
            return result;
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public Page<IssueDTO> findPage(int page, int pageSize) {
        try {
            String countQuery = "SELECT COUNT(*) FROM Issues";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(countQuery);
            if (!resultSet.next()) {
                throw new RepositoryException("Failed to count records in Issues table for paging.");
            }
            long count = resultSet.getLong(1);
            resultSet.close();

            String query =
                    "SELECT IssueID, Title, PublicationDate, Publisher, SeriesNumber, Description " +
                            "FROM Issues LIMIT ? OFFSET ?";


            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, pageSize);
            preparedStatement.setInt(2, page * pageSize);
            resultSet = preparedStatement.executeQuery();

            List<IssueDTO> data = new ArrayList<>(pageSize);
            while (resultSet.next()) {
                data.add(mapRow(resultSet));
            }

            return new Page<>(data, page, pageSize, count);
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public IssueDTO save(IssueDTO issueDTOToSave) {
        try {
            if (exists(issueDTOToSave.getId(), connection)) {
                return updateIssue(issueDTOToSave, connection);
            } else {
                return createIssue(issueDTOToSave, connection);
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }

    private boolean exists(Long issueId, Connection connection) throws SQLException {
        if (issueId == null) {
            return false;
        }

        String query = "SELECT COUNT(*) FROM Issues WHERE IssueId = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setLong(1, issueId);
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            throw new RepositoryException("Failed to count records in Issues table for existence check.");
        }
        long count = resultSet.getLong(1);
        resultSet.close();
        return count > 0;
    }

    private IssueDTO updateIssue(IssueDTO issueDTOToSave, Connection connection) throws SQLException {
        String query = "UPDATE Issues SET " +
                "Title = '" + issueDTOToSave.getTitle() + "', " +
                "PublicationDate = '" + issueDTOToSave.getPublicationDate().toString() + "', " +
                "Publisher = '" + issueDTOToSave.getPublisher() + "', " +
                "SeriesNumber = " + issueDTOToSave.getSeriesNumber() + ", " +
                "Description = '" + issueDTOToSave.getDescription() + "' " +
                "WHERE IssueId = " + issueDTOToSave.getId();

        // TODO: Exercise 1 (JDBC) - Make SQL injection safe
        Statement statement = connection.createStatement();
        int updated = statement.executeUpdate(query);
        if (updated != 1) {
            throw new RepositoryException("Failed to update Issue " + issueDTOToSave.getId() +
                    " - expected 1 updated row, but got " + updated);
        }
        return issueDTOToSave;
    }

    private IssueDTO createIssue(IssueDTO issueDTOToSave, Connection connection) throws SQLException {
        String query = "INSERT INTO Issues(Title, PublicationDate, Publisher, SeriesNumber, Description)" +
                " VALUES(?, ?, ?, ?, ?);";
        PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, issueDTOToSave.getTitle());
        statement.setDate(2, Date.valueOf(issueDTOToSave.getPublicationDate()));
        statement.setString(3, issueDTOToSave.getPublisher());
        statement.setInt(4, issueDTOToSave.getSeriesNumber());
        statement.setString(5, issueDTOToSave.getDescription());
        int inserted = statement.executeUpdate();

        if (inserted != 1) {
            throw new RepositoryException("Failed to insert Issue - expected 1 added row, but got " + inserted);
        }

        ResultSet resultSet = statement.getGeneratedKeys();
        if (!resultSet.next()) {
            throw new RepositoryException("Failed to retrieve generated key on insert...");
        }
        issueDTOToSave.setId(resultSet.getLong(1));
        return issueDTOToSave;
    }

    private IssueDTO mapRow(ResultSet resultSet) throws SQLException {
        IssueDTO issueDto = new IssueDTO();
        issueDto.setId((long) resultSet.getInt(1));
        issueDto.setTitle(resultSet.getString(2));
        issueDto.setPublicationDate(resultSet.getDate(3).toLocalDate());
        issueDto.setPublisher(resultSet.getString(4));
        issueDto.setSeriesNumber(resultSet.getInt(5));
        issueDto.setDescription(resultSet.getString(6));
        return issueDto;
    }
}
