package za.co.entelect.bootcamp.orm.jdbc;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.gateway.exception.RepositoryException;
import za.co.entelect.bootcamp.orm.core.gateway.exception.NotFoundException;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;

import java.sql.*;

@SuppressWarnings("SqlResolve")

public class IssueJdbcRepositoryNoAutoClose implements ReadOneIssueInterface {
    private final String url;

    IssueJdbcRepositoryNoAutoClose(String url) {
        this.url = url;
    }

    @Override
    public IssueDTO findById(Long id) {
        Connection connection = null;
        String query =
                "SELECT IssueID, Title, PublicationDate, Publisher," +
                      " SeriesNumber, Description " +
                "FROM Issues " +
                "WHERE IssueId = " + id;
        IssueDTO issueDto;
        try {
            // Any of these may throw SQLException
            connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            if (!resultSet.next()) { // Could throw SQLException
                throw new NotFoundException(IssueDTO.class, id);
            }

            issueDto = new IssueDTO();
            // Any of these may throw
            issueDto.setId((long) resultSet.getInt(1));
            issueDto.setTitle(resultSet.getString(2));
            issueDto.setPublicationDate(resultSet.getDate(3).toLocalDate());
            issueDto.setPublisher(resultSet.getString(4));
            issueDto.setSeriesNumber(resultSet.getInt(5));
            issueDto.setDescription(resultSet.getString(6));
        } catch (SQLException e) {
            throw new RepositoryException(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close(); // Even this could throw!
                }
            } catch (SQLException ex) {
                // It only failed to close, so probably swallow this and return?
            }
        }

        return issueDto;
    }
}
