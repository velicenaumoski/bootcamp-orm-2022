package za.co.entelect.bootcamp.orm.integration.jpa.example;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readpage.ReadIssuePageInterface;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadIssuePageInterfaceTest;
import za.co.entelect.bootcamp.orm.jpa.example.IssueJpaRepository;

public class JpaReadIssuePageInterfaceTest extends AbstractReadIssuePageInterfaceTest {
    @Override
    protected ReadIssuePageInterface getClassUnderTest() throws Exception {
        return new IssueJpaRepository();
    }
}
