package za.co.entelect.bootcamp.orm.integration.jpa.exercise;

import za.co.entelect.bootcamp.orm.core.usecases.stock.readone.ReadOneStockInterface;
import za.co.entelect.bootcamp.orm.integration.core.stock.AbstractReadOneStockInterfaceTest;
import za.co.entelect.bootcamp.orm.jpa.exercise.StockJpaRepository;

public class JpaReadOneStockInterfaceTest extends AbstractReadOneStockInterfaceTest {
    @Override
    protected ReadOneStockInterface getClassUnderTest() throws Exception {
        return new StockJpaRepository();
    }
}
