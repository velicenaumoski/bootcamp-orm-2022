package za.co.entelect.bootcamp.orm.integration.jpa.exercise;

import za.co.entelect.bootcamp.orm.core.usecases.stock.readpage.ReadStockPageInterface;
import za.co.entelect.bootcamp.orm.integration.core.stock.AbstractReadStockPageInterfaceTest;
import za.co.entelect.bootcamp.orm.jpa.exercise.StockJpaRepository;

public class JpaReadStockPageInterfaceTest extends AbstractReadStockPageInterfaceTest {
    @Override
    protected ReadStockPageInterface getClassUnderTest() throws Exception {
        return new StockJpaRepository();
    }
}
