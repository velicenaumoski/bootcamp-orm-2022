package za.co.entelect.bootcamp.orm.integration.jdbc;

import org.junit.Before;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractSaveIssueInterfaceTest;
import za.co.entelect.bootcamp.orm.jdbc.IssueJdbcRepository;

public class JdbcSaveIssueInterfaceTest extends AbstractSaveIssueInterfaceTest {

    private IssueJdbcRepository gateway;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        this.gateway = new IssueJdbcRepository(dbProvider.getConnection());
    }

    @Override
    protected SaveIssueInterface getClassUnderTest() throws Exception {
        return this.gateway;
    }

    @Override
    protected ReadOneIssueInterface getReadGateway() throws Exception {
        return this.gateway;
    }
}
