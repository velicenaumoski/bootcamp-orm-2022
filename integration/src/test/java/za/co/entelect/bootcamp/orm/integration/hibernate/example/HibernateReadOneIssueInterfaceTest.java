package za.co.entelect.bootcamp.orm.integration.hibernate.example;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.hibernate.example.IssueHibernateRepository;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadOneIssueInterfaceTest;

public class HibernateReadOneIssueInterfaceTest extends AbstractReadOneIssueInterfaceTest {
    @Override
    protected ReadOneIssueInterface getClassUnderTest() throws Exception {
        return new IssueHibernateRepository();
    }
}
