package za.co.entelect.bootcamp.orm.integration.hibernate.example;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readpage.ReadIssuePageInterface;
import za.co.entelect.bootcamp.orm.hibernate.example.IssueHibernateRepository;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadIssuePageInterfaceTest;

public class HibernateReadIssuePageInterfaceTest extends AbstractReadIssuePageInterfaceTest {
    @Override
    protected ReadIssuePageInterface getClassUnderTest() throws Exception {
        return new IssueHibernateRepository();
    }
}
