package za.co.entelect.bootcamp.orm.integration.jdbc;

import org.junit.Test;
import za.co.entelect.bootcamp.orm.core.common.Page;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.gateway.exception.RepositoryException;

import static org.junit.Assert.assertEquals;

public class IssueJdbcRepositorySecurityTest extends AbstractJdbcRepositoryTest {

    // Failed on purpose: you need to fix the implementation to prevent SQL injection
    @Test
    public void testSqlInjectionIsNotPossible() throws RepositoryException {
        // Given
        Page<IssueDTO> issues = cut.findPage(0, 5);
        IssueDTO issueDTOToUpdate = issues.getData().get(0);
        assertEquals(9L, issues.getTotalRecords());

        // When
        issueDTOToUpdate.setDescription("So many quotes!");
        issueDTOToUpdate.setTitle("BobbyTables' WHERE IssueId=" + issueDTOToUpdate.getId() + "; DROP TABLE Issues; --");
        cut.save(issueDTOToUpdate);

        // Then
        Page<IssueDTO> afterDrop = cut.findPage(0, 5); // Throws Table "ISSUES" not found
    }
}
