package za.co.entelect.bootcamp.orm.integration.jdbc;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readpage.ReadIssuePageInterface;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadIssuePageInterfaceTest;
import za.co.entelect.bootcamp.orm.jdbc.IssueJdbcRepository;

public class JdbcReadIssuePageInterfaceTest extends AbstractReadIssuePageInterfaceTest {
    @Override
    protected ReadIssuePageInterface getClassUnderTest() throws Exception {
        return new IssueJdbcRepository(dbProvider.getConnection());
    }
}
