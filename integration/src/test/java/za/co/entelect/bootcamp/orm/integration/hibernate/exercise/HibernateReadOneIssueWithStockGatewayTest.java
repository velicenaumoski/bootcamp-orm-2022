package za.co.entelect.bootcamp.orm.integration.hibernate.exercise;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.hibernate.exercise.IssueWithStockHibernateRepository;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadOneIssueInterfaceTest;

public class HibernateReadOneIssueWithStockGatewayTest extends AbstractReadOneIssueInterfaceTest {
    @Override
    protected ReadOneIssueInterface getClassUnderTest() throws Exception {
        return new IssueWithStockHibernateRepository();
    }
}
