package za.co.entelect.bootcamp.orm.integration.jpa.example;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadOneIssueInterfaceTest;
import za.co.entelect.bootcamp.orm.jpa.example.IssueJpaRepository;

public class JpaReadOneIssueInterfaceTest extends AbstractReadOneIssueInterfaceTest {
    @Override
    protected ReadOneIssueInterface getClassUnderTest() throws Exception {
        return new IssueJpaRepository();
    }
}
