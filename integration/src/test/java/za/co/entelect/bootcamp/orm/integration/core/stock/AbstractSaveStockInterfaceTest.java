package za.co.entelect.bootcamp.orm.integration.core.stock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;
import za.co.entelect.bootcamp.orm.core.enums.Condition;
import za.co.entelect.bootcamp.orm.core.usecases.stock.readone.ReadOneStockInterface;
import za.co.entelect.bootcamp.orm.core.usecases.stock.save.SaveStockInterface;
import za.co.entelect.bootcamp.orm.integration.common.InMemoryDbProvider;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;


public abstract class AbstractSaveStockInterfaceTest {
    protected abstract SaveStockInterface getClassUnderTest() throws Exception;
    protected abstract ReadOneStockInterface getReadGateway() throws Exception;

    protected InMemoryDbProvider dbProvider;

    @Before
    public void setUp() throws SQLException, Exception {
        dbProvider = new InMemoryDbProvider("comic.sql", "MODE=MSSQLServer");
    }

    @After
    public void tearDown() throws Exception {
        dbProvider.cleanUpDatabase();
    }

    @Test
    public void testCreate() throws Exception {
        // Given
        IssueDTO issueDto = new IssueDTO();
        issueDto.setId(2L);

        // When
        StockDTO stockDto = new StockDTO();
        stockDto.setIssueDto(issueDto);
        stockDto.setAvailableQty((short) 2);
        stockDto.setCondition(Condition.Average);
        stockDto.setCoverImagePath("C:\\Images\\batman.png");
        stockDto.setPrice(BigDecimal.valueOf(100.0));
        stockDto.setComments("For great justice!");
        stockDto = getClassUnderTest().save(stockDto);

        // Then
        assertNotNull(stockDto.getId());

        assertEquals(10L, (long) countRecords());
    }

    @Test
    public void testUpdate() throws Exception {
        // Given
        StockDTO stockDto = getReadGateway().findById(1L);

        // When
        stockDto.setComments("Test");
        stockDto.setPrice(new BigDecimal("123.00"));
        stockDto.setAvailableQty((short) 1);
        stockDto.setCoverImagePath("C:\\Images\\superman.png");
        stockDto.setCondition(Condition.Poor);
        stockDto = getClassUnderTest().save(stockDto);

        // Then
        assertValues(stockDto);
    }

    Long countRecords() throws SQLException {
        Statement statement = dbProvider.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM Stock");

        if (!resultSet.next()) {
            return 0L;
        }

        return resultSet.getLong(1);
    }

    protected void assertValues(StockDTO saved) throws SQLException {
        String query = "SELECT condition, availableQty, price, comments, coverImagePath FROM Stock WHERE StockReferenceID = ?";
        PreparedStatement statement = dbProvider.getConnection().prepareStatement(query);
        statement.setLong(1, saved.getId());

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();

        assertTrue(resultSet.getString("condition").contains(saved.getCondition().name()));
        assertEquals((short) saved.getAvailableQty(), resultSet.getShort("availableQty"));
        assertEquals(saved.getPrice(), resultSet.getBigDecimal("price"));
        assertEquals(saved.getComments(), resultSet.getString("comments"));
        assertEquals(saved.getCoverImagePath(), resultSet.getString("coverImagePath"));
    }
}
