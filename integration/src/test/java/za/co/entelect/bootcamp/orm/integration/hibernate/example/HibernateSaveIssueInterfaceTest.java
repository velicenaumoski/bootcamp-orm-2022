package za.co.entelect.bootcamp.orm.integration.hibernate.example;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;
import za.co.entelect.bootcamp.orm.hibernate.example.IssueHibernateRepository;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractSaveIssueInterfaceTest;

import java.sql.SQLException;

public class HibernateSaveIssueInterfaceTest extends AbstractSaveIssueInterfaceTest {
    private IssueHibernateRepository gateway;

    @Override
    public void setUp() throws SQLException, Exception {
        super.setUp();

        this.gateway = new IssueHibernateRepository();
    }

    @Override
    protected SaveIssueInterface getClassUnderTest() throws Exception {
        return gateway;
    }

    @Override
    protected ReadOneIssueInterface getReadGateway() throws Exception {
        return gateway;
    }
}
