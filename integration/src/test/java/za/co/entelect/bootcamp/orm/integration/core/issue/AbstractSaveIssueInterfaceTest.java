package za.co.entelect.bootcamp.orm.integration.core.issue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;
import za.co.entelect.bootcamp.orm.integration.common.InMemoryDbProvider;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public abstract class AbstractSaveIssueInterfaceTest {
    protected abstract SaveIssueInterface getClassUnderTest() throws Exception;
    protected abstract ReadOneIssueInterface getReadGateway() throws Exception;

    protected InMemoryDbProvider dbProvider;

    @Before
    public void setUp() throws SQLException, Exception {
        dbProvider = new InMemoryDbProvider("comic.sql", "MODE=MSSQLServer");
    }

    @After
    public void tearDown() throws Exception {
        dbProvider.cleanUpDatabase();
    }

    @Test
    public void testCreate() throws Exception {
        // Given
        // When
        IssueDTO issueDto = new IssueDTO();
        issueDto.setTitle("Test");
        issueDto.setDescription("Test");
        issueDto.setSeriesNumber(5);
        issueDto.setPublicationDate(LocalDate.now());
        issueDto.setPublisher("Entelect");
        issueDto = getClassUnderTest().save(issueDto);

        // Then
        assertNotNull(issueDto.getId());

        assertEquals(10L, (long) countRecords());
    }

    @Test
    public void testUpdate() throws Exception {
        // Given
        IssueDTO issueDto = getReadGateway().findById(2L);

        // When
        issueDto.setTitle("Test 2");
        issueDto.setDescription("Test 2");
        issueDto.setPublisher("Entelect");
        issueDto.setPublicationDate(LocalDate.now());
        issueDto.setSeriesNumber(999);
        issueDto = getClassUnderTest().save(issueDto);

        // Then
        assertValues(issueDto);
    }

    Long countRecords() throws SQLException {
        Statement statement = dbProvider.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM Issues");

        if (!resultSet.next()) {
            return 0L;
        }

        return resultSet.getLong(1);
    }

    protected void assertValues(IssueDTO saved) throws SQLException {
        String query = "SELECT title, description, publisher, publicationDate, seriesNumber FROM Issues WHERE IssueID = ?";
        PreparedStatement statement = dbProvider.getConnection().prepareStatement(query);
        statement.setLong(1, saved.getId());

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();

        assertEquals(saved.getTitle(), resultSet.getString("title"));
        assertEquals(saved.getDescription(), resultSet.getString("description"));
        assertEquals(saved.getPublisher(), resultSet.getString("publisher"));
        assertEquals(saved.getPublicationDate(), resultSet.getDate("publicationDate").toLocalDate());
        assertEquals(saved.getSeriesNumber().intValue(), resultSet.getInt("seriesNumber"));
    }
}
