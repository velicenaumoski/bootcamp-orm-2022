package za.co.entelect.bootcamp.orm.integration.jpa.exercise;

import za.co.entelect.bootcamp.orm.core.usecases.stock.readone.ReadOneStockInterface;
import za.co.entelect.bootcamp.orm.core.usecases.stock.save.SaveStockInterface;
import za.co.entelect.bootcamp.orm.integration.core.stock.AbstractSaveStockInterfaceTest;
import za.co.entelect.bootcamp.orm.jpa.exercise.StockJpaRepository;

import java.sql.SQLException;

public class JpaSaveStockInterfaceTest extends AbstractSaveStockInterfaceTest {
    private StockJpaRepository gateway;

    @Override
    public void setUp() throws SQLException, Exception {
        super.setUp();

        this.gateway = new StockJpaRepository();
    }

    @Override
    protected SaveStockInterface getClassUnderTest() throws Exception {
        return gateway;
    }

    @Override
    protected ReadOneStockInterface getReadGateway() throws Exception {
        return gateway;
    }
}
