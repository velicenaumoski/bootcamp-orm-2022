package za.co.entelect.bootcamp.orm.integration.jpa.example;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractSaveIssueInterfaceTest;
import za.co.entelect.bootcamp.orm.jpa.example.IssueJpaRepository;

import java.sql.SQLException;

public class JpaSaveIssueInterfaceTest extends AbstractSaveIssueInterfaceTest {
    private IssueJpaRepository gateway;

    @Override
    public void setUp() throws SQLException, Exception {
        super.setUp();

        this.gateway = new IssueJpaRepository();
    }

    @Override
    protected SaveIssueInterface getClassUnderTest() throws Exception {
        return gateway;
    }

    @Override
    protected ReadOneIssueInterface getReadGateway() throws Exception {
        return gateway;
    }
}
