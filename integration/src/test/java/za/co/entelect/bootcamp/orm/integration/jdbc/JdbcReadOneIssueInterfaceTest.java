package za.co.entelect.bootcamp.orm.integration.jdbc;

import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.integration.core.issue.AbstractReadOneIssueInterfaceTest;
import za.co.entelect.bootcamp.orm.jdbc.IssueJdbcRepository;

public class JdbcReadOneIssueInterfaceTest extends AbstractReadOneIssueInterfaceTest {
    @Override
    protected ReadOneIssueInterface getClassUnderTest() {
        return new IssueJdbcRepository(dbProvider.getConnection());
    }
}
