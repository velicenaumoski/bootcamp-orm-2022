package za.co.entelect.bootcamp.orm.jpa.example;

import za.co.entelect.bootcamp.orm.core.common.Page;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.gateway.exception.NotFoundException;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readpage.ReadIssuePageInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;
import za.co.entelect.bootcamp.orm.jpa.example.mappers.IssueMapper;
import za.co.entelect.bootcamp.orm.jpa.example.entities.IssueEntity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class IssueJpaRepository implements ReadOneIssueInterface, ReadIssuePageInterface, SaveIssueInterface {
    private EntityManager entityManager;
    private IssueMapper issueMapper;

    public IssueJpaRepository() {
        issueMapper = new IssueMapper();
        entityManager = Persistence.
                createEntityManagerFactory("comicPersistenceUnit").
                createEntityManager();
    }

    @Override
    public IssueDTO findById(Long id) {
        IssueEntity issueEntity = entityManager.find(IssueEntity.class, id.intValue());
        if (issueEntity == null) {
            entityManager.getTransaction().rollback();
            throw new NotFoundException(IssueDTO.class, id);
        }

        return issueMapper.toIssueDTO(issueEntity);
    }

    @Override
    public Page<IssueDTO> findPage(int page, int pageSize) {
        // Get the page data
        Query query = entityManager.createQuery("select issue from IssueEntity issue");
        query.setFirstResult(page * pageSize);
        query.setMaxResults(pageSize);
        List<IssueEntity> issueEntities = query.getResultList();

        // Count the total records
        Query countQuery = entityManager.createQuery("select count (issue.id) from IssueEntity issue");
        Long countResult = (Long) countQuery.getSingleResult();

        return new Page<IssueDTO>(
                issueMapper.toIssueDTOs(issueEntities),
                page,
                pageSize,
                countResult
        );
    }

    @Override
    public IssueDTO save(IssueDTO issueDto) {
        entityManager.getTransaction().begin();

        IssueEntity issueEntity;
        if (issueDto.getId() == null) {
            issueEntity = issueMapper.toIssueEntity(issueDto);
        } else {
            issueEntity = entityManager.find(IssueEntity.class, issueDto.getId().intValue());
            issueMapper.updateEntity(issueEntity, issueDto);
        }
        entityManager.persist(issueEntity);
        entityManager.getTransaction().commit();

        return issueMapper.toIssueDTO(issueEntity);
    }
}
