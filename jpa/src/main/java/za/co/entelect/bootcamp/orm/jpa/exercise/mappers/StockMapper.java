package za.co.entelect.bootcamp.orm.jpa.exercise.mappers;

import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;
import za.co.entelect.bootcamp.orm.jpa.exercise.entities.StockEntity;

import java.util.ArrayList;
import java.util.List;

public class StockMapper {

    public StockDTO toStockDTOs(StockEntity stockEntity) {
        if (stockEntity == null) {
            return null;
        }

        StockDTO stockDTO = new StockDTO();

        if (stockEntity.getId() != null) {
            stockDTO.setId(stockEntity.getId().longValue());
        }
        stockDTO.setCondition(stockEntity.getCondition());
        stockDTO.setAvailableQty(stockEntity.getAvailableQty());
        stockDTO.setPrice(stockEntity.getPrice());
        stockDTO.setComments(stockEntity.getComments());
        stockDTO.setCoverImagePath(stockEntity.getCoverImagePath());

        return stockDTO;
    }

    public StockEntity toStockEntity(StockDTO stockDto) {
        if (stockDto == null) {
            return null;
        }

        StockEntity stockEntity = new StockEntity();

        if (stockDto.getId() != null) {
            stockEntity.setId(stockDto.getId().intValue());
        }
        stockEntity.setCondition(stockDto.getCondition());
        stockEntity.setAvailableQty(stockDto.getAvailableQty());
        stockEntity.setPrice(stockDto.getPrice());
        stockEntity.setComments(stockDto.getComments());
        stockEntity.setCoverImagePath(stockDto.getCoverImagePath());

        return stockEntity;
    }

    public List<StockDTO> toStockDTOs(List<StockEntity> stockEntities) {
        if (stockEntities == null) {
            return null;
        }

        List<StockDTO> list = new ArrayList<StockDTO>(stockEntities.size());
        for (StockEntity stockEntity : stockEntities) {
            list.add(toStockDTOs(stockEntity));
        }

        return list;
    }

    public void updateEntity(StockEntity stockEntity, StockDTO stockDto) {
        if (stockDto == null) {
            return;
        }

        if (stockDto.getId() != null) {
            stockEntity.setId(stockDto.getId().intValue());
        }
        stockEntity.setCondition(stockDto.getCondition());
        stockEntity.setAvailableQty(stockDto.getAvailableQty());
        stockEntity.setPrice(stockDto.getPrice());
        stockEntity.setComments(stockDto.getComments());
        stockEntity.setCoverImagePath(stockDto.getCoverImagePath());
    }
}
