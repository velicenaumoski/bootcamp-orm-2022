package za.co.entelect.bootcamp.orm.jpa.exercise.entities;

import lombok.Getter;
import lombok.Setter;
import za.co.entelect.bootcamp.orm.core.enums.Condition;
import za.co.entelect.bootcamp.orm.jpa.common.models.IdentifiableDataModel;
import za.co.entelect.bootcamp.orm.jpa.example.entities.IssueEntity;

import java.math.BigDecimal;

// TODO: Exercise 3 (JPA) - Still needs mapping annotations - see comic.sql in integration for DB schema

@Getter
@Setter
public class StockEntity extends IdentifiableDataModel {
    private Condition condition;
    private Short availableQty;
    private BigDecimal price;
    private String comments;
    private String coverImagePath;

    private IssueEntity issue;
}
