package za.co.entelect.bootcamp.orm.jpa.exercise;

import za.co.entelect.bootcamp.orm.core.common.Page;
import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;
import za.co.entelect.bootcamp.orm.core.usecases.stock.readone.ReadOneStockInterface;
import za.co.entelect.bootcamp.orm.core.usecases.stock.readpage.ReadStockPageInterface;
import za.co.entelect.bootcamp.orm.core.usecases.stock.save.SaveStockInterface;
import za.co.entelect.bootcamp.orm.jpa.exercise.mappers.StockMapper;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class StockJpaRepository implements ReadOneStockInterface, ReadStockPageInterface, SaveStockInterface {
    private EntityManager entityManager;
    private StockMapper stockMapper;

    public StockJpaRepository() {
        stockMapper = new StockMapper();
        entityManager = Persistence.
                createEntityManagerFactory("comicPersistenceUnit").
                createEntityManager();
    }

    @Override
    public StockDTO findById(Long id) {
        // TODO: Exercise 3 (JPA) - Implement StockJpaRepository.findById
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<StockDTO> findPage(int page, int pageSize) {
        // TODO: Exercise 3 (JPA) - Implement StockJpaRepository.findPage
        throw new UnsupportedOperationException();
    }

    @Override
    public StockDTO save(StockDTO issue) {
        // TODO: Exercise 3 (JPA) - Implement StockJpaRepository.save
        throw new UnsupportedOperationException();
    }
}
