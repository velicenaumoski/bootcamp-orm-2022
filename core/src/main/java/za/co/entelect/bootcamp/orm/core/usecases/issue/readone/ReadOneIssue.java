package za.co.entelect.bootcamp.orm.core.usecases.issue.readone;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.usecases.common.Presenter;
import za.co.entelect.bootcamp.orm.core.usecases.common.UseCase;

public class ReadOneIssue<Model> extends UseCase<Model, ReadOneIssueRequest, ReadOneIssueResponse> {
    private final ReadOneIssueInterface gateway;

    public ReadOneIssue(
            Presenter<Model, ReadOneIssueResponse> presenter,
            ReadOneIssueInterface gateway
    ) {
        super(presenter);

        this.gateway = gateway;
    }

    @Override
    public Model execute(ReadOneIssueRequest request) {
        IssueDTO issueDto = gateway.findById(request.getIssueId());
        return presenter.present(buildResponse(issueDto));
    }

    private ReadOneIssueResponse buildResponse(IssueDTO issueDto) {
        ReadOneIssueResponse response = new ReadOneIssueResponse();
        response.setIssueDto(issueDto);
        return response;
    }
}
