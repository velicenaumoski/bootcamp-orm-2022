package za.co.entelect.bootcamp.orm.core.usecases.stock.save;

import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;
import za.co.entelect.bootcamp.orm.core.usecases.common.Presenter;
import za.co.entelect.bootcamp.orm.core.usecases.common.UseCase;

public class SaveStock<Model> extends UseCase<Model, SaveStockRequest, SaveStockResponse> {
    private final SaveStockInterface gateway;

    public SaveStock(
            Presenter<Model, SaveStockResponse> presenter,
            SaveStockInterface gateway
    ) {
        super(presenter);

        this.gateway = gateway;
    }

    @Override
    public Model execute(SaveStockRequest request) {
        StockDTO stockDto = gateway.save(request.getStockDto());
        return presenter.present(buildResponse(stockDto));
    }

    private SaveStockResponse buildResponse(StockDTO stockDto) {
        SaveStockResponse response = new SaveStockResponse();
        response.setStockDto(stockDto);
        return response;
    }
}
