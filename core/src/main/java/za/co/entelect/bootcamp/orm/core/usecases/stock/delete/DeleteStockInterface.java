package za.co.entelect.bootcamp.orm.core.usecases.stock.delete;

import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;

public interface DeleteStockInterface {
    void delete(StockDTO stockDto);
}
