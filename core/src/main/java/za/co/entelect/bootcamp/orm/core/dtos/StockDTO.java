package za.co.entelect.bootcamp.orm.core.dtos;

import lombok.Getter;
import lombok.Setter;
import za.co.entelect.bootcamp.orm.core.common.Identifiable;
import za.co.entelect.bootcamp.orm.core.enums.Condition;

import java.math.BigDecimal;

@Getter
@Setter
public class StockDTO extends Identifiable {
    private Condition condition;
    private Short availableQty;
    private BigDecimal price;
    private String comments;
    private String coverImagePath;

    private IssueDTO issueDto;
}
