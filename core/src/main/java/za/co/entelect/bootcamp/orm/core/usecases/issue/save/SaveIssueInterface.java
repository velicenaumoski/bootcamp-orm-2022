package za.co.entelect.bootcamp.orm.core.usecases.issue.save;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;

public interface SaveIssueInterface {
    IssueDTO save(IssueDTO issueDto);
}
