package za.co.entelect.bootcamp.orm.core.usecases.stock.readone;

import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;

public interface ReadOneStockInterface {
    StockDTO findById(Long id);
}
