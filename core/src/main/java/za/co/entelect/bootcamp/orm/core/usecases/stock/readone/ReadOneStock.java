package za.co.entelect.bootcamp.orm.core.usecases.stock.readone;

import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;
import za.co.entelect.bootcamp.orm.core.usecases.common.Presenter;
import za.co.entelect.bootcamp.orm.core.usecases.common.UseCase;

public class ReadOneStock<Model> extends UseCase<Model, ReadOneStockRequest, ReadOneStockResponse> {
    private final ReadOneStockInterface gateway;

    public ReadOneStock(
            Presenter<Model, ReadOneStockResponse> presenter,
            ReadOneStockInterface gateway
    ) {
        super(presenter);

        this.gateway = gateway;
    }

    @Override
    public Model execute(ReadOneStockRequest request) {
        StockDTO stockDto = gateway.findById(request.getStockId());
        return presenter.present(buildResponse(stockDto));
    }

    private ReadOneStockResponse buildResponse(StockDTO stockDto) {
        ReadOneStockResponse response = new ReadOneStockResponse();
        response.setStockDto(stockDto);
        return response;
    }
}
