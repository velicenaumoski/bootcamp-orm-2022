package za.co.entelect.bootcamp.orm.core.usecases.issue.readpage;

import za.co.entelect.bootcamp.orm.core.common.Page;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;

public interface ReadIssuePageInterface {
    Page<IssueDTO> findPage(int page, int pageSize);
}
