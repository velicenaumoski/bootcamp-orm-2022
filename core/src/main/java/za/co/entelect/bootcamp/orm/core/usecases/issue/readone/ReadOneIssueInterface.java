package za.co.entelect.bootcamp.orm.core.usecases.issue.readone;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;

public interface ReadOneIssueInterface {
    IssueDTO findById(Long id);
}
