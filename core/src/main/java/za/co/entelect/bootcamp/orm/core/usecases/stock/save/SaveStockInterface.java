package za.co.entelect.bootcamp.orm.core.usecases.stock.save;

import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;

public interface SaveStockInterface {
    StockDTO save(StockDTO issue);
}
