package za.co.entelect.bootcamp.orm.core.usecases.issue.delete;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;

public interface DeleteIssueInterface {
    void delete(IssueDTO issueDto);
}
