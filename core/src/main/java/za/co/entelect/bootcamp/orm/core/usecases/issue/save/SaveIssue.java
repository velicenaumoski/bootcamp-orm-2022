package za.co.entelect.bootcamp.orm.core.usecases.issue.save;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.usecases.common.Presenter;
import za.co.entelect.bootcamp.orm.core.usecases.common.UseCase;

public class SaveIssue<Model> extends UseCase<Model, SaveIssueRequest, SaveIssueResponse> {
    private final SaveIssueInterface gateway;

    public SaveIssue(
            Presenter<Model, SaveIssueResponse> presenter,
            SaveIssueInterface gateway
    ) {
        super(presenter);

        this.gateway = gateway;
    }

    @Override
    public Model execute(SaveIssueRequest request) {
        IssueDTO issueDto = gateway.save(request.getIssueDto());
        return presenter.present(buildResponse(issueDto));
    }

    private SaveIssueResponse buildResponse(IssueDTO issueDto) {
        SaveIssueResponse response = new SaveIssueResponse();
        response.setIssueDto(issueDto);
        return response;
    }
}
