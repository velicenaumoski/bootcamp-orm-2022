package za.co.entelect.bootcamp.orm.core.usecases.stock.save;

import lombok.Getter;
import lombok.Setter;
import za.co.entelect.bootcamp.orm.core.dtos.StockDTO;
import za.co.entelect.bootcamp.orm.core.usecases.common.UseCaseRequest;

@Getter
@Setter
public class SaveStockRequest implements UseCaseRequest {
    private StockDTO stockDto;
}
