package za.co.entelect.bootcamp.orm.core.dtos;

import za.co.entelect.bootcamp.orm.core.common.Identifiable;
import za.co.entelect.bootcamp.orm.core.enums.Country;

public class CreatorDTO extends Identifiable {
    private String name;
    private String email;
    private Country countryOfOrigin;
    private byte[] taxReferenceNumber;
}
