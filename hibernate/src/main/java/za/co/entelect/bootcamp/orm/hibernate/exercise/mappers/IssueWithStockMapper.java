package za.co.entelect.bootcamp.orm.hibernate.exercise.mappers;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.hibernate.exercise.entities.IssueWithStockEntity;

import java.util.ArrayList;
import java.util.List;

public class IssueWithStockMapper {

    public IssueDTO toIssueEntity(IssueWithStockEntity issueDataModel) {
        if (issueDataModel == null) {
            return null;
        }

        IssueDTO issueDTO = new IssueDTO();

        if (issueDataModel.getId() != null) {
            issueDTO.setId(issueDataModel.getId().longValue());
        }
        issueDTO.setVersion(issueDataModel.getVersion());
        issueDTO.setTitle(issueDataModel.getTitle());
        issueDTO.setPublisher(issueDataModel.getPublisher());
        issueDTO.setPublicationDate(issueDataModel.getPublicationDate());
        if (issueDataModel.getSeriesNumber() != null) {
            issueDTO.setSeriesNumber(issueDataModel.getSeriesNumber().intValue());
        }
        issueDTO.setDescription(issueDataModel.getDescription());

        return issueDTO;
    }

    public IssueWithStockEntity toIssueWithStockEntity(IssueDTO issueDto) {
        if (issueDto == null) {
            return null;
        }

        IssueWithStockEntity issueWithStockEntity = new IssueWithStockEntity();

        if (issueDto.getId() != null) {
            issueWithStockEntity.setId(issueDto.getId().intValue());
        }
        issueWithStockEntity.setVersion(issueDto.getVersion());
        issueWithStockEntity.setTitle(issueDto.getTitle());
        issueWithStockEntity.setPublisher(issueDto.getPublisher());
        issueWithStockEntity.setPublicationDate(issueDto.getPublicationDate());
        if (issueDto.getSeriesNumber() != null) {
            issueWithStockEntity.setSeriesNumber(issueDto.getSeriesNumber().shortValue());
        }
        issueWithStockEntity.setDescription(issueDto.getDescription());

        return issueWithStockEntity;
    }

    public List<IssueDTO> toIssueDTOs(List<IssueWithStockEntity> issueDataModels) {
        if (issueDataModels == null) {
            return null;
        }

        List<IssueDTO> list = new ArrayList<IssueDTO>(issueDataModels.size());
        for (IssueWithStockEntity issueWithStockEntity : issueDataModels) {
            list.add(toIssueEntity(issueWithStockEntity));
        }

        return list;
    }
}
