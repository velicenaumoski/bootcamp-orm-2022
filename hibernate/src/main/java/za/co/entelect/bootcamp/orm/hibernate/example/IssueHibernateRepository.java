package za.co.entelect.bootcamp.orm.hibernate.example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import za.co.entelect.bootcamp.orm.core.common.Page;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.gateway.exception.NotFoundException;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readpage.ReadIssuePageInterface;
import za.co.entelect.bootcamp.orm.core.usecases.issue.save.SaveIssueInterface;
import za.co.entelect.bootcamp.orm.hibernate.example.mappers.IssueMapper;
import za.co.entelect.bootcamp.orm.hibernate.example.entities.IssueEntity;

import java.util.List;

public class IssueHibernateRepository implements ReadOneIssueInterface, ReadIssuePageInterface, SaveIssueInterface {
    private SessionFactory sessionFactory;
    private IssueMapper issueMapper;

    public IssueHibernateRepository() throws Exception {
        issueMapper = new IssueMapper();
        final StandardServiceRegistry registry =
                new StandardServiceRegistryBuilder()
                        .configure("/example/hibernate.cfg.xml")
                        .build();

        try {
            sessionFactory = new MetadataSources(registry)
                    .buildMetadata()
                    .buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory,
            // but being in this catch means we had trouble building the SessionFactory
            // so destroy it manually...
            StandardServiceRegistryBuilder.destroy(registry);
            throw e;
        }
    }

    @Override
    public IssueDTO findById(Long id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        IssueEntity issueEntity = session.get(IssueEntity.class, id.intValue());

        if (issueEntity == null) {
            throw new NotFoundException(IssueDTO.class, id);
        }

        return issueMapper.toIssueDTO(issueEntity);
    }

    @Override
    public Page<IssueDTO> findPage(int page, int pageSize) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        // Get the page data
        Query query = session.createQuery("from IssueEntity");
        query.setFirstResult(page * pageSize);
        query.setMaxResults(pageSize);
        List<IssueEntity> pageData = query.list();

        // Count the total records
        Query countQuery = session
                .createQuery("select count (i.id) from IssueEntity i");
        Long countResult = (Long) countQuery.uniqueResult();

        return new Page<>(
                issueMapper.toIssueDTOs(pageData),
                page,
                pageSize,
                countResult
        );
    }

    @Override
    public IssueDTO save(IssueDTO issueDto) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        IssueEntity dataModel = issueMapper.toIssueEntity(issueDto);
        session.saveOrUpdate(dataModel);
        session.getTransaction().commit();

        return issueMapper.toIssueDTO(dataModel);
    }
}
