package za.co.entelect.bootcamp.orm.hibernate.exercise;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.core.gateway.exception.NotFoundException;
import za.co.entelect.bootcamp.orm.core.usecases.issue.readone.ReadOneIssueInterface;
import za.co.entelect.bootcamp.orm.hibernate.exercise.entities.IssueWithStockEntity;
import za.co.entelect.bootcamp.orm.hibernate.exercise.mappers.IssueWithStockMapper;

public class IssueWithStockHibernateRepository implements ReadOneIssueInterface {
    private SessionFactory sessionFactory;
    private IssueWithStockMapper issueWithStockMapper;

    public IssueWithStockHibernateRepository() throws Exception {
        issueWithStockMapper = new IssueWithStockMapper();
        final StandardServiceRegistry registry =
                new StandardServiceRegistryBuilder()
                        .configure("/exercise/hibernate.cfg.xml")
                        .build();

        try {
            sessionFactory = new MetadataSources(registry)
                    .buildMetadata()
                    .buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory,
            // but being in this catch means we had trouble building the SessionFactory
            // so destroy it manually...
            StandardServiceRegistryBuilder.destroy(registry);
            throw e;
        }
    }

    @Override
    public IssueDTO findById(Long id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        IssueWithStockEntity issueDataModel = session.get(IssueWithStockEntity.class, id.intValue());

        if (issueDataModel == null) {
            throw new NotFoundException(IssueDTO.class, id);
        }

        return issueWithStockMapper.toIssueEntity(issueDataModel);
    }
}
