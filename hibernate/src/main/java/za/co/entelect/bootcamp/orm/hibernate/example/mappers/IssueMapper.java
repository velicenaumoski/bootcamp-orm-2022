package za.co.entelect.bootcamp.orm.hibernate.example.mappers;

import za.co.entelect.bootcamp.orm.core.dtos.IssueDTO;
import za.co.entelect.bootcamp.orm.hibernate.example.entities.IssueEntity;

import java.util.ArrayList;
import java.util.List;

public class IssueMapper {

    public IssueDTO toIssueDTO(IssueEntity issueEntity) {
        if (issueEntity == null) {
            return null;
        }

        IssueDTO issueDTO = new IssueDTO();

        if (issueEntity.getId() != null) {
            issueDTO.setId(issueEntity.getId().longValue());
        }
        issueDTO.setVersion(issueEntity.getVersion());
        issueDTO.setTitle(issueEntity.getTitle());
        issueDTO.setPublisher(issueEntity.getPublisher());
        issueDTO.setPublicationDate(issueEntity.getPublicationDate());
        if (issueEntity.getSeriesNumber() != null) {
            issueDTO.setSeriesNumber(issueEntity.getSeriesNumber().intValue());
        }
        issueDTO.setDescription(issueEntity.getDescription());

        return issueDTO;
    }

    public IssueEntity toIssueEntity(IssueDTO issueDto) {
        if (issueDto == null) {
            return null;
        }

        IssueEntity issueEntity = new IssueEntity();

        if (issueDto.getId() != null) {
            issueEntity.setId(issueDto.getId().intValue());
        }
        issueEntity.setVersion(issueDto.getVersion());
        issueEntity.setTitle(issueDto.getTitle());
        issueEntity.setPublisher(issueDto.getPublisher());
        issueEntity.setPublicationDate(issueDto.getPublicationDate());
        if (issueDto.getSeriesNumber() != null) {
            issueEntity.setSeriesNumber(issueDto.getSeriesNumber().shortValue());
        }
        issueEntity.setDescription(issueDto.getDescription());

        return issueEntity;
    }

    public List<IssueDTO> toIssueDTOs(List<IssueEntity> issueEntities) {
        if (issueEntities == null) {
            return null;
        }

        List<IssueDTO> list = new ArrayList<IssueDTO>(issueEntities.size());
        for (IssueEntity issueEntity : issueEntities) {
            list.add(toIssueDTO(issueEntity));
        }

        return list;
    }
}
